import lejos.nxt.Button;
import lejos.nxt.Motor;
import lejos.nxt.Sound;

public class DanceChoreo1 {

	public static void main(String[] args) {
		Button.ENTER.waitForPressAndRelease();
		// 1sec vorw�rts
		Motor.A.setSpeed(360);
		Motor.B.setSpeed(360);

		Motor.A.forward();
		Motor.B.forward();
		Sound.playNote(Sound.PIANO, 264, 1100);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
		// 1 sec r�ckw�rts
		Motor.A.backward();
		Motor.B.backward();
		Sound.playNote(Sound.PIANO, 396, 1000);
		

		// 1sec vorw�rts
		Motor.A.forward();
		Motor.B.forward();
		Sound.playNote(Sound.PIANO, 440, 1000);
		

		Motor.A.stop(true);
		Motor.B.stop();

		// Kreis f�r 7.68 sec
		Motor.A.setSpeed(361);
		Motor.B.setSpeed(180);
		Motor.A.forward();
		Motor.B.forward();
		Sound.playNote(Sound.PIANO, 440, 3840);
		Sound.playNote(Sound.PIANO, 264, 3840);
		Sound.playNote(Sound.PIANO, 528, 3840);
		

		// Drehen rechts
		Motor.A.setSpeed(360);
		Motor.B.setSpeed(360);
		Motor.A.backward();
		Motor.B.forward();
		Sound.playNote(Sound.PIANO, 528, 2500);
		
		// Drehen links
		Motor.A.forward();
		Motor.B.backward();
		Sound.playNote(Sound.PIANO, 330, 2500);
		
		// Drehen rechts
		Motor.A.backward();
		Motor.B.forward();
		Sound.playNote(Sound.PIANO, 528, 2500);
		
		// Drehen links
		Motor.A.forward();
		Motor.B.backward();
		Sound.playNote(Sound.PIANO, 330, 2500);
		
		//vorw�rts
		Motor.A.forward();
		Motor.B.forward();
		Sound.playNote(Sound.PIANO, 396, 3500);
		
		//
		
		
	}
}
