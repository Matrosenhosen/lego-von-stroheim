import lejos.nxt.Button;
import lejos.nxt.Motor;
import lejos.nxt.Sound;

public class DanceTwo {
	static int c = 264;
	static int d = 297;
	static int e = 330;
	static int f = 349;
	static int g = 396;
	static int a = 440;
	static int h = 493;
	static int d2 = 587;
	static int gis = 415;
	static int c2 = 523;
	static int ais = 233;
	static int d3 = 1174;
	static int a2 = 880;
	static int gis2 = 830;
	static int g2 = 783;
	static int f2 = 698;
	static int c3 = 1046;
	public static void main(String[] args) {
		Button.ENTER.waitForPressAndRelease();
		//Vorw�rts
		Motor.A.setSpeed(360);
		Motor.B.setSpeed(360);
		Motor.A.forward();
		Motor.B.forward();
		Sound.playNote(Sound.PIANO,d,200);
		//R�ckw�rts
		Motor.A.backward();
		Motor.B.backward();
		Sound.playNote(Sound.PIANO,d,200);
		//Vorw�rts
		Motor.A.forward();
		Motor.B.forward();
		Sound.playNote(Sound.PIANO,d2,300);
		//R�ckw�rts
		Motor.A.backward();
		Motor.B.backward();
		Sound.playNote(Sound.PIANO,a,450);
		//Drehen
		Motor.A.backward();
		Motor.B.forward();
		Sound.playNote(Sound.PIANO, gis,400);
		//Drehen
		Motor.A.forward();
		Motor.B.backward();
		Sound.playNote(Sound.PIANO,g,270);
		//Drehen
		Motor.A.backward();
		Motor.B.forward();
		Sound.playNote(Sound.PIANO,f,350);
		//Vorw�rts
		Motor.A.forward();
		Motor.B.forward();
		Sound.playNote(Sound.PIANO,d,300);
		//R�ckw�rts
		Motor.A.backward();
		Motor.B.backward();
		Sound.playNote(Sound.PIANO, f,300);
		//Vorw�rs
		Motor.A.forward();
		Motor.B.forward();
		Sound.playNote(Sound.PIANO, g,300);
		
			//Zweite Runde
				//Vorw�rts
				Motor.A.setSpeed(360);
				Motor.B.setSpeed(360);
				Motor.A.forward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO,c,200);
				//R�ckw�rts
				Motor.A.backward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO,c,200);
				//Vorw�rts
				Motor.A.forward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO,d2,300);
				//R�ckw�rts
				Motor.A.backward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO,a,450);
				//Drehen
				Motor.A.backward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, gis,400);
				//Drehen
				Motor.A.forward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO,g,270);
				//Drehen
				Motor.A.backward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO,f,350);
				//Vorw�rts
				Motor.A.forward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO,d,300);
				//R�ckw�rts
				Motor.A.backward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO, f,300);
				//Vorw�rs
				Motor.A.forward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, g,300);
				
			//Dritte Runde
				//Vorw�rts
				Motor.A.setSpeed(360);
				Motor.B.setSpeed(360);
				Motor.A.forward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO,d,200);
				//R�ckw�rts
				Motor.A.backward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO,d,200);
				//Vorw�rts
				Motor.A.forward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO,d2,300);
				//R�ckw�rts
				Motor.A.backward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO,a,450);
				//Drehen
				Motor.A.backward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, gis,400);
				//Drehen
				Motor.A.forward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO,g,270);
				//Drehen
				Motor.A.backward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO,f,350);
				//Vorw�rts
				Motor.A.forward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO,d,300);
				//R�ckw�rts
				Motor.A.backward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO, f,300);
				//Vorw�rs
				Motor.A.forward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, g,300);
				
			//Vierte Runde
				//Vorw�rts
				Motor.A.setSpeed(360);
				Motor.B.setSpeed(360);
				Motor.A.forward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO,c,200);
				//R�ckw�rts
				Motor.A.backward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO,c,200);
				//Vorw�rts
				Motor.A.forward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO,d2,300);
				//R�ckw�rts
				Motor.A.backward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO,a,450);
				//Drehen
				Motor.A.backward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, gis,400);
				//Drehen
				Motor.A.forward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO,g,270);
				//Drehen
				Motor.A.backward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO,f,350);
				//Vorw�rts
				Motor.A.forward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO,d,300);
				//R�ckw�rts
				Motor.A.backward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO, f,300);
				//Vorw�rs
				Motor.A.forward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, g,300);
				
			//5. Runde
				//R�ckw�rts 
				Motor.A.backward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO, d2, 200);
				//Vorw�rts
				Motor.A.forward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, d2, 200);
				//R�ckw�rs
				Motor.A.backward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO, d3, 300);
				//Vorw�rts
				Motor.A.forward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, a2, 450);
				//Drehen
				Motor.A.forward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO, gis2, 400);
				//Drehen
				Motor.A.backward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, g2, 270);
				//Drehen
				Motor.A.forward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO, f2, 350);
				//R�ckw�rts
				Motor.A.backward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO, d2, 300);
				//Vorw�rts
				Motor.A.forward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, f2, 300);
				//R�ckw�rts
				Motor.A.backward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO, g2, 300);
				
			//6. Runde
				//R�ckw�rts 
				Motor.A.backward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO, c2, 200);
				//Vorw�rts
				Motor.A.forward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, c2, 200);
				//R�ckw�rs
				Motor.A.backward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO, d3, 300);
				//Vorw�rts
				Motor.A.forward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, a2, 450);
				//Drehen
				Motor.A.forward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO, gis2, 400);
				//Drehen
				Motor.A.backward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, g2, 270);
				//Drehen
				Motor.A.forward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO, f2, 350);
				//R�ckw�rts
				Motor.A.backward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO, d2, 300);
				//Vorw�rts
				Motor.A.forward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, f2, 300);
				//R�ckw�rts
				Motor.A.backward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO, g2, 300);
					
			//7. Runde
				//R�ckw�rts 
				Motor.A.backward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO, d2, 200);
				//Vorw�rts
				Motor.A.forward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, d2, 200);
				//R�ckw�rs
				Motor.A.backward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO, d3, 300);
				//Vorw�rts
				Motor.A.forward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, a2, 450);
				//Drehen
				Motor.A.forward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO, gis2, 400);
				//Drehen
				Motor.A.backward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, g2, 270);
				//Drehen
				Motor.A.forward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO, f2, 350);
				//R�ckw�rts
				Motor.A.backward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO, d2, 300);
				//Vorw�rts
				Motor.A.forward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, f2, 300);
				//R�ckw�rts
				Motor.A.backward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO, g2, 300);

			//7. Runde
				//R�ckw�rts 
				Motor.A.backward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO, c2, 200);
				//Vorw�rts
				Motor.A.forward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, c2, 200);
				//R�ckw�rs
				Motor.A.backward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO, d3, 300);
				//Vorw�rts
				Motor.A.forward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, a2, 450);
				//Drehen
				Motor.A.forward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO, gis2, 400);
				//Drehen
				Motor.A.backward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, g2, 270);
				//Drehen
				Motor.A.forward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO, f2, 350);
				//R�ckw�rts
				Motor.A.backward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO, d2, 300);
				//Vorw�rts
				Motor.A.forward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, f2, 300);
				//R�ckw�rts
				Motor.A.backward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO, g2, 300);

				
				//Vorw�rts
				Motor.A.forward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, f2, 300);
				//R�ckw�rts
				Sound.playNote(Sound.PIANO, f2, 300);
				Motor.A.backward();
				Motor.B.backward();
				//Vorw�rts
				Motor.A.forward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, f2, 300);
				//R�ckw�rts
				Sound.playNote(Sound.PIANO, f2, 300);
				Motor.A.backward();
				Motor.B.backward();
				//Vorw�rts
				Motor.A.forward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, f2, 300);
				//Drehen
				Motor.A.forward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO, d2, 350);
				//Drehen
				Motor.A.backward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, d2, 350);
				//R�ckw�rts
				Sound.playNote(Sound.PIANO, f2, 300);
				Motor.A.backward();
				Motor.B.backward();
				//Vorw�rts
				Motor.A.forward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, f2, 300);
				//R�ckw�rts
				Sound.playNote(Sound.PIANO, f2, 300);
				Motor.A.backward();
				Motor.B.backward();
				//Drehen
				Motor.A.backward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, g2, 300);
				Sound.playNote(Sound.PIANO, gis2, 350);
				Sound.playNote(Sound.PIANO, g2, 300);
				//Drehen
				Motor.A.forward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO, f2, 300);
				Sound.playNote(Sound.PIANO, d2, 300);
				Sound.playNote(Sound.PIANO, f2, 300);
				Sound.playNote(Sound.PIANO, g2, 350);
				//Vorw�rts
				Sound.playNote(Sound.PIANO, f2, 300);
				Motor.A.forward();
				Motor.B.forward();
				//R�ckw�rts
				Sound.playNote(Sound.PIANO, f2, 300);
				Motor.A.backward();
				Motor.B.backward();
				//Vorw�rts
				Sound.playNote(Sound.PIANO, f2, 300);
				Motor.A.forward();
				Motor.B.forward();
				//Drehen
				Motor.A.forward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO, g2, 350);
				Sound.playNote(Sound.PIANO, gis2, 350);
				//Drehen
				Motor.A.backward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, a2, 300);
				Sound.playNote(Sound.PIANO, c3, 300);
				Sound.playNote(Sound.PIANO, a2, 300);
				//R�ckw�rts
				Sound.playNote(Sound.PIANO, d3, 400);
				Motor.A.backward();
				Motor.B.backward();
				//Vorw�rts
				Sound.playNote(Sound.PIANO, d3, 400);
				Motor.A.forward();
				Motor.B.forward();
				//R�ckw�rts
				Sound.playNote(Sound.PIANO, d3, 400);
				Motor.A.backward();
				Motor.B.backward();
				//Drehen
				Motor.A.forward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO, a2, 300);
				//Drehen
				Motor.A.backward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, d3, 400);
				//Drehen
				Motor.A.forward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO, c3, 500);
				
				//Vorw�rts
				Motor.A.forward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, f2, 300);
				//R�ckw�rts
				Sound.playNote(Sound.PIANO, f2, 300);
				Motor.A.backward();
				Motor.B.backward();
				//Vorw�rts
				Motor.A.forward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, f2, 300);
				//R�ckw�rts
				Sound.playNote(Sound.PIANO, f2, 300);
				Motor.A.backward();
				Motor.B.backward();
				//Vorw�rts
				Motor.A.forward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, f2, 300);
				//Drehen
				Motor.A.forward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO, d2, 350);
				//Drehen
				Motor.A.backward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, d2, 350);
				//R�ckw�rts
				Sound.playNote(Sound.PIANO, f2, 300);
				Motor.A.backward();
				Motor.B.backward();
				//Vorw�rts
				Motor.A.forward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, f2, 300);
				//R�ckw�rts
				Sound.playNote(Sound.PIANO, f2, 300);
				Motor.A.backward();
				Motor.B.backward();
				//Drehen
				Motor.A.backward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, g2, 300);
				Sound.playNote(Sound.PIANO, gis2, 350);
				Sound.playNote(Sound.PIANO, g2, 300);
				//Drehen
				Motor.A.forward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO, f2, 300);
				Sound.playNote(Sound.PIANO, d2, 300);
				Sound.playNote(Sound.PIANO, f2, 300);
				Sound.playNote(Sound.PIANO, g2, 350);
				//Vorw�rts
				Sound.playNote(Sound.PIANO, f2, 300);
				Motor.A.forward();
				Motor.B.forward();
				//R�ckw�rts
				Sound.playNote(Sound.PIANO, f2, 300);
				Motor.A.backward();
				Motor.B.backward();
				//Vorw�rts
				Sound.playNote(Sound.PIANO, f2, 300);
				Motor.A.forward();
				Motor.B.forward();
				//Drehen
				Motor.A.forward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO, g2, 350);
				Sound.playNote(Sound.PIANO, gis2, 350);
				//Drehen
				Motor.A.backward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, a2, 300);
				Sound.playNote(Sound.PIANO, c3, 300);
				Sound.playNote(Sound.PIANO, a2, 300);
				//R�ckw�rts
				Sound.playNote(Sound.PIANO, d3, 400);
				Motor.A.backward();
				Motor.B.backward();
				//Vorw�rts
				Sound.playNote(Sound.PIANO, d3, 400);
				Motor.A.forward();
				Motor.B.forward();
				//R�ckw�rts
				Sound.playNote(Sound.PIANO, d3, 400);
				Motor.A.backward();
				Motor.B.backward();
				//Drehen
				Motor.A.forward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO, a2, 300);
				//Drehen
				Motor.A.backward();
				Motor.B.forward();
				Sound.playNote(Sound.PIANO, d3, 400);
				//Drehen
				Motor.A.forward();
				Motor.B.backward();
				Sound.playNote(Sound.PIANO, c3, 500);
				
				Motor.A.stop(true);
				Motor.B.stop();
				
		

	}

}
