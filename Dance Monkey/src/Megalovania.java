import lejos.nxt.Button;
import lejos.nxt.Sound;

public class Megalovania {
	static int c = 264;
	static int d = 297;
	static int e = 330;
	static int f = 349;
	static int g = 396;
	static int a = 440;
	static int h = 493;
	static int d2 = 587;
	static int gis = 415;
	static int c2 = 523;
	static int ais = 233;
	static int d3 = 1174;
	static int a2 = 880;
	static int gis2 = 830;
	static int g2 = 783;
	static int f2 = 698;
	static int c3 = 1046;
	static int ais2 = 466;

	public static void main(String[] args) {
		// Megalovania
		Button.ENTER.waitForPressAndRelease();
		Sound.playNote(Sound.PIANO, d, 200);
		Sound.playNote(Sound.PIANO, d, 200);
		Sound.playNote(Sound.PIANO, d2, 300);
		Sound.playNote(Sound.PIANO, a, 450);
		Sound.playNote(Sound.PIANO, gis, 400);
		Sound.playNote(Sound.PIANO, g, 270);
		Sound.playNote(Sound.PIANO, f, 350);
		Sound.playNote(Sound.PIANO, d, 300);
		Sound.playNote(Sound.PIANO, f, 300);
		Sound.playNote(Sound.PIANO, g, 300);

		Sound.playNote(Sound.PIANO, c, 200);
		Sound.playNote(Sound.PIANO, c, 200);
		Sound.playNote(Sound.PIANO, d2, 300);
		Sound.playNote(Sound.PIANO, a, 450);
		Sound.playNote(Sound.PIANO, gis, 400);
		Sound.playNote(Sound.PIANO, g, 270);
		Sound.playNote(Sound.PIANO, f, 350);
		Sound.playNote(Sound.PIANO, d, 300);
		Sound.playNote(Sound.PIANO, f, 300);
		Sound.playNote(Sound.PIANO, g, 300);

		Sound.playNote(Sound.PIANO, ais, 200);
		Sound.playNote(Sound.PIANO, ais, 200);
		Sound.playNote(Sound.PIANO, d2, 300);
		Sound.playNote(Sound.PIANO, a, 450);
		Sound.playNote(Sound.PIANO, gis, 400);
		Sound.playNote(Sound.PIANO, g, 270);
		Sound.playNote(Sound.PIANO, f, 350);
		Sound.playNote(Sound.PIANO, d, 300);
		Sound.playNote(Sound.PIANO, f, 300);
		Sound.playNote(Sound.PIANO, g, 300);

		Sound.playNote(Sound.PIANO, d, 200);
		Sound.playNote(Sound.PIANO, d, 200);
		Sound.playNote(Sound.PIANO, d2, 300);
		Sound.playNote(Sound.PIANO, a, 450);
		Sound.playNote(Sound.PIANO, gis, 400);
		Sound.playNote(Sound.PIANO, g, 270);
		Sound.playNote(Sound.PIANO, f, 350);
		Sound.playNote(Sound.PIANO, d, 300);
		Sound.playNote(Sound.PIANO, f, 300);
		Sound.playNote(Sound.PIANO, g, 300);
		
		Sound.playNote(Sound.PIANO, c, 200);
		Sound.playNote(Sound.PIANO, c, 200);
		Sound.playNote(Sound.PIANO, d2, 300);
		Sound.playNote(Sound.PIANO, a, 450);
		Sound.playNote(Sound.PIANO, gis, 400);
		Sound.playNote(Sound.PIANO, g, 270);
		Sound.playNote(Sound.PIANO, f, 350);
		Sound.playNote(Sound.PIANO, d, 300);
		Sound.playNote(Sound.PIANO, f, 300);
		Sound.playNote(Sound.PIANO, g, 300);
		
		Sound.playNote(Sound.PIANO, ais, 200);
		Sound.playNote(Sound.PIANO, ais, 200);
		Sound.playNote(Sound.PIANO, d2, 300);
		Sound.playNote(Sound.PIANO, a, 450);
		Sound.playNote(Sound.PIANO, gis, 400);
		Sound.playNote(Sound.PIANO, g, 270);
		Sound.playNote(Sound.PIANO, f, 350);
		Sound.playNote(Sound.PIANO, d, 300);
		Sound.playNote(Sound.PIANO, f, 300);
		Sound.playNote(Sound.PIANO, g, 300);

		Sound.playNote(Sound.PIANO, d2, 200);
		Sound.playNote(Sound.PIANO, d2, 200);
		Sound.playNote(Sound.PIANO, d3, 300);
		Sound.playNote(Sound.PIANO, a2, 450);
		Sound.playNote(Sound.PIANO, gis2, 400);
		Sound.playNote(Sound.PIANO, g2, 270);
		Sound.playNote(Sound.PIANO, f2, 350);
		Sound.playNote(Sound.PIANO, d2, 300);
		Sound.playNote(Sound.PIANO, f2, 300);
		Sound.playNote(Sound.PIANO, g2, 300);

		Sound.playNote(Sound.PIANO, c2, 200);
		Sound.playNote(Sound.PIANO, c2, 200);
		Sound.playNote(Sound.PIANO, d3, 300);
		Sound.playNote(Sound.PIANO, a2, 450);
		Sound.playNote(Sound.PIANO, gis2, 400);
		Sound.playNote(Sound.PIANO, g2, 270);
		Sound.playNote(Sound.PIANO, f2, 350);
		Sound.playNote(Sound.PIANO, d2, 300);
		Sound.playNote(Sound.PIANO, f2, 300);
		Sound.playNote(Sound.PIANO, g2, 300);

		Sound.playNote(Sound.PIANO, ais2, 200);
		Sound.playNote(Sound.PIANO, ais2, 200);
		Sound.playNote(Sound.PIANO, d3, 300);
		Sound.playNote(Sound.PIANO, a2, 450);
		Sound.playNote(Sound.PIANO, gis2, 400);
		Sound.playNote(Sound.PIANO, g2, 270);
		Sound.playNote(Sound.PIANO, f2, 350);
		Sound.playNote(Sound.PIANO, d2, 300);
		Sound.playNote(Sound.PIANO, f2, 300);
		Sound.playNote(Sound.PIANO, g2, 300);
		
		Sound.playNote(Sound.PIANO, d2, 200);
		Sound.playNote(Sound.PIANO, d2, 200);
		Sound.playNote(Sound.PIANO, d3, 300);
		Sound.playNote(Sound.PIANO, a2, 450);
		Sound.playNote(Sound.PIANO, gis2, 400);
		Sound.playNote(Sound.PIANO, g2, 270);
		Sound.playNote(Sound.PIANO, f2, 350);
		Sound.playNote(Sound.PIANO, d2, 300);
		Sound.playNote(Sound.PIANO, f2, 300);
		Sound.playNote(Sound.PIANO, g2, 300);

		Sound.playNote(Sound.PIANO, c2, 200);
		Sound.playNote(Sound.PIANO, c2, 200);
		Sound.playNote(Sound.PIANO, d3, 300);
		Sound.playNote(Sound.PIANO, a2, 450);
		Sound.playNote(Sound.PIANO, gis2, 400);
		Sound.playNote(Sound.PIANO, g2, 270);
		Sound.playNote(Sound.PIANO, f2, 350);
		Sound.playNote(Sound.PIANO, d2, 300);
		Sound.playNote(Sound.PIANO, f2, 300);
		Sound.playNote(Sound.PIANO, g2, 300);
		
		Sound.playNote(Sound.PIANO, ais2, 200);
		Sound.playNote(Sound.PIANO, ais2, 200);
		Sound.playNote(Sound.PIANO, d3, 300);
		Sound.playNote(Sound.PIANO, a2, 450);
		Sound.playNote(Sound.PIANO, gis2, 400);
		Sound.playNote(Sound.PIANO, g2, 270);
		Sound.playNote(Sound.PIANO, f2, 350);
		Sound.playNote(Sound.PIANO, d2, 300);
		Sound.playNote(Sound.PIANO, f2, 300);
		Sound.playNote(Sound.PIANO, g2, 300);

		Sound.playNote(Sound.PIANO, f2, 300);
		Sound.playNote(Sound.PIANO, f2, 300);
		Sound.playNote(Sound.PIANO, f2, 300);
		Sound.playNote(Sound.PIANO, f2, 300);
		Sound.playNote(Sound.PIANO, f2, 300);
		Sound.playNote(Sound.PIANO, d2, 350);
		Sound.playNote(Sound.PIANO, d2, 350);
		Sound.playNote(Sound.PIANO, f2, 300);
		Sound.playNote(Sound.PIANO, f2, 300);
		Sound.playNote(Sound.PIANO, f2, 300);
		Sound.playNote(Sound.PIANO, g2, 300);
		Sound.playNote(Sound.PIANO, gis2, 350);
		Sound.playNote(Sound.PIANO, g2, 300);
		Sound.playNote(Sound.PIANO, f2, 300);
		Sound.playNote(Sound.PIANO, d2, 300);
		Sound.playNote(Sound.PIANO, f2, 300);
		Sound.playNote(Sound.PIANO, g2, 350);
		Sound.playNote(Sound.PIANO, f2, 300);
		Sound.playNote(Sound.PIANO, f2, 300);
		Sound.playNote(Sound.PIANO, f2, 300);
		Sound.playNote(Sound.PIANO, g2, 350);
		Sound.playNote(Sound.PIANO, gis2, 350);
		Sound.playNote(Sound.PIANO, a2, 300);
		Sound.playNote(Sound.PIANO, c3, 300);
		Sound.playNote(Sound.PIANO, a2, 300);
		Sound.playNote(Sound.PIANO, d3, 400);
		Sound.playNote(Sound.PIANO, d3, 400);
		Sound.playNote(Sound.PIANO, d3, 400);
		Sound.playNote(Sound.PIANO, a2, 300);
		Sound.playNote(Sound.PIANO, d3, 400);
		Sound.playNote(Sound.PIANO, c3, 500);

	}
}