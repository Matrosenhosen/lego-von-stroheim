import lejos.nxt.Sound;

public class PokemonDance {
static int c = 264;
static int d = 297;
static int e = 330;
static int f = 349;
static int g = 396;
static int a = 440;
static int h = 493;
static int d2 = 587;
static int b = 466;

	public static void main(String[] args) {
		Sound.playNote(Sound.PIANO,440 , 300);
		Sound.playNote(Sound.PIANO,440 , 300);
		Sound.playNote(Sound.PIANO,440 , 300);
		Sound.playNote(Sound.PIANO,440 , 300);
		Sound.playNote(Sound.PIANO,440 , 300);
		Sound.playNote(Sound.PIANO,391 , 300);
		Sound.playNote(Sound.PIANO,293 , 300);
		Sound.playNote(Sound.PIANO,264, 300);
		Sound.playNote(Sound.PIANO,264 , 300);
		
		//Megalovania
		Sound.playNote(Sound.PIANO,d,200);
		Sound.playNote(Sound.PIANO,d,200);
		Sound.playNote(Sound.PIANO,d2,400);
		Sound.playNote(Sound.PIANO,a,300);
		Sound.playNote(Sound.PIANO, b, 500);
		Sound.playNote(Sound.PIANO,g,500);
		Sound.playNote(Sound.PIANO,f,500);
		Sound.playNote(Sound.PIANO,d,400);
		Sound.playNote(Sound.PIANO, f, 300);
		Sound.playNote(Sound.PIANO, g, 300);
	
	}
	
}
