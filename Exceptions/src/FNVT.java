import java.util.Random;

import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.LightSensor;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;

public class FNVT {
	static SensorPort light = SensorPort.S1;
	static LightSensor l = new LightSensor(light);

	public static void main(String[] args) {
		l.readValue();
		Button.ENTER.waitForPressAndRelease();

		Motor.A.setSpeed(360);
		Motor.B.setSpeed(353.6f);

		while (true) {
			try {
				while (l.readValue() > 30) {
					fahreVorwaertsStrecke(15);
					linksRechtsdrehen();
				}
				LCD.drawInt(l.readValue(), 0, 0);
				throw new Exception();
			} catch (Exception e) {
				rueckwaerts();
			}
		}
	}

	public static void rueckwaerts() {
		Motor.A.backward();
		Motor.B.backward();

		try {
			Thread.sleep(750);
		} catch (InterruptedException e1) {
		}
		Random r = new Random();
		if (r.nextInt(2) == 0) {
			dreheRechts(r.nextInt(360) + 1);
		} else {
			dreheLinks(r.nextInt(360) + 1);
		}
	}

	public static void dreheRechts(int grad) {
		Motor.A.backward();
		Motor.B.forward();
		grad = grad * 100;
		grad = grad * 1933;
		grad = grad / 360;
		grad = grad / 10000;
		for (int i = 0; i <= 100; i++)
			try {
				Thread.sleep(grad);
				if (l.readValue() < 30) {
					throw new Exception();
				}
			} catch (InterruptedException e) {
			} catch (Exception e) {
				rueckwaerts();
			}
	}

	public static void dreheLinks(int grad) {
		Motor.B.backward();
		Motor.A.forward();
		grad = grad * 100;
		grad = grad * 1933;
		grad = grad / 360;
		grad = grad / 10000;
		for (int i = 0; i <= 100; i++)
			try {
				Thread.sleep(grad);
				if (l.readValue() < 30) {
					throw new Exception();
				}
			} catch (InterruptedException e) {
			} catch (Exception e) {
				rueckwaerts();
			}
	}

	// 1Meter = 5870
	public static void fahreVorwaertsStrecke(int streckeInCm) {
		streckeInCm = streckeInCm * 5870;
		streckeInCm = streckeInCm / 100;
		fahreVorwaertsZeit(streckeInCm);
	}

	public static void fahreVorwaertsZeit(float zeitInSek) {
		fahreVorwaerts();
		zeitInSek = zeitInSek/100;
		for (int i = 0; i < 100; i++) {
			try {
				Thread.sleep((int) zeitInSek);
				if (l.readValue() < 30) {
					throw new Exception();
				}
			} catch (InterruptedException e) {
			} catch (Exception e) {
				rueckwaerts();
			}
		}
		Motor.A.stop(true);
		Motor.B.stop();
	}

	public static void fahreVorwaerts() {
		Motor.A.setSpeed(360);
		Motor.B.setSpeed(353.6f);
		Motor.A.forward();
		Motor.B.forward();
	}

	public static void linksRechtsdrehen() {
		dreheLinks(30);
		try {
			Thread.sleep(50);
		} catch (InterruptedException e) {
		}
		dreheRechts(60);
		dreheLinks(30);
	}
}
