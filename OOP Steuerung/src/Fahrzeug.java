import lejos.nxt.LCD;
import lejos.nxt.Motor;
import lejos.nxt.NXTRegulatedMotor;
import lejos.nxt.SensorPort;
import lejos.nxt.TouchSensor;

public class Fahrzeug implements FahrzeugInterface, AnzeigeInterface, TouchInterface {
	private NXTRegulatedMotor rechterMotor;
	private NXTRegulatedMotor linkerMotor;
	private final float durchmesserReifen = 5.4f;
	private final float abstandAchse = 8.6f;
	private final int drehung = 3920;
	private TouchSensor ts;

	public Fahrzeug() {
		this.linkerMotor = Motor.B;
		this.rechterMotor = Motor.A;
		this.setGeschwindigkeit(353.6f, 360);
	}

	public Fahrzeug(float geschwindigkeitLinks, float geschwindigkeitRechts) {
		this.linkerMotor = Motor.B;
		this.rechterMotor = Motor.A;

		this.setGeschwindigkeit(geschwindigkeitLinks, geschwindigkeitRechts);
		this.ts = new TouchSensor(SensorPort.S2);
	}

	public float getDurchmesserReifen() {
		return this.durchmesserReifen;
	}

	public float getAbstandAchse() {
		return this.abstandAchse;
	}

	public float getGeschwindigkeitRechts() {
		return this.rechterMotor.getSpeed();
	}

	public float getGeschwindigkeitLinks() {
		return this.linkerMotor.getSpeed();
	}

	public void setGeschwindigkeit(float links, float rechts) {
		this.linkerMotor.setSpeed(links);
		this.rechterMotor.setSpeed(rechts);
	}

	public void fahreVorwaerts() {
		this.linkerMotor.forward();
		this.rechterMotor.forward();
	}

	// 1Meter = 5870
	public void fahreVorwaertsStrecke(int streckeInCm) {
		int[] v = { linkerMotor.getSpeed(), rechterMotor.getSpeed() };
		this.setGeschwindigkeit(353.6f, 360);
		streckeInCm = streckeInCm * 5870;
		streckeInCm = streckeInCm / 100;
		fahreVorwaertsZeit(streckeInCm);
		setGeschwindigkeit(v[0], v[1]);
	}

	public void fahreVorwaertsZeit(float zeitInSek) {
		fahreVorwaerts();
		try {
			Thread.sleep((int) zeitInSek);
		} catch (InterruptedException e) {
		}
		this.stoppe();
	}

	public void fahreRueckwaerts() {
		this.linkerMotor.backward();
		this.rechterMotor.backward();
	}

	public void fahreRueckwaertsStrecke(int streckeInCm) {
		int[] v = { linkerMotor.getSpeed(), rechterMotor.getSpeed() };
		this.setGeschwindigkeit(353.6f, 360);
		streckeInCm = streckeInCm * 5870;
		streckeInCm = streckeInCm / 100;
		fahreRueckwaertsZeit(streckeInCm);
		setGeschwindigkeit(v[0], v[1]);
	}

	public void fahreRueckwaertsZeit(float zeitInSek) {
		this.fahreRueckwaerts();
		try {
			Thread.sleep((int) zeitInSek * 1000);
		} catch (InterruptedException e) {
		}
		this.stoppe();
	}

	public void dreheRechts() {

		this.linkerMotor.forward();
		this.rechterMotor.backward();
	}

	public void dreheRechtsGrad(int winkel) {
		int[] v = { linkerMotor.getSpeed(), rechterMotor.getSpeed() };
		this.setGeschwindigkeit(180, 180);
		float a = winkel;
		a = a * 100;
		a = a * drehung;
		a = a / 360;
		a = a / 100;
		dreheRechtsZeit(a);
		setGeschwindigkeit(v[0], v[1]);
	}

	public void dreheRechtsZeit(float zeitInSek) {
		dreheRechts();
		try {
			Thread.sleep((int) zeitInSek);
		} catch (InterruptedException e) {
		}
		this.stoppe();
	}

	public void dreheLinks() {
		this.setGeschwindigkeit(180, 180);
		this.linkerMotor.backward();
		this.rechterMotor.forward();
	}

	public void dreheLinksGrad(int winkel) {
		float a = winkel;
		a += 1.5f;
		a = a * 100;
		a = a * drehung;
		a = a / 360;
		a = a / 100;
		if (winkel < 20)
			a += 11;
		dreheLinksZeit(a);
		zeigeZahl(a);
	}

	public void dreheLinksZeit(float zeitInMSek) {
		dreheLinks();
		try {
			Thread.sleep((int) zeitInMSek);
		} catch (InterruptedException e) {
		}
		this.stoppe();
	}

	public void stoppe() {
		this.linkerMotor.stop(true);
		this.rechterMotor.stop();
	}

	public void beschleunige(int zeitInMs) {
		linkerMotor.setAcceleration(60);
		rechterMotor.setAcceleration(60);
		fahreVorwaerts();
		try {
			Thread.sleep(zeitInMs);
		} catch (InterruptedException e) {
		}
		linkerMotor.setAcceleration(6000);
		rechterMotor.setAcceleration(6000);
	}

	public void zeigeText(String text) {

	}

	public void zeigeZahl(int zahl) {
		LCD.drawInt(zahl, 0, 0);
	}

	@Override
	public void zeigeZahl(float zahl) {
		zeigeZahl((int) zahl);

	}

	@Override
	public boolean istGedrueckt() {
		return ts.isPressed();
	}
}
