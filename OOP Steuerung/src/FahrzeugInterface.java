
public interface FahrzeugInterface {
	
	public float getDurchmesserReifen();
	
	public float getAbstandAchse();
	
	public float getGeschwindigkeitLinks();
	
	public float getGeschwindigkeitRechts();
	
	public void setGeschwindigkeit(float links, float rechts);
	
	public void fahreVorwaerts();
	
	public void fahreVorwaertsStrecke(int streckeInCm);
	
	public void fahreVorwaertsZeit(float zeitInSek);
	
	public void fahreRueckwaerts();
	
	public void fahreRueckwaertsStrecke(int streckeInCm);
	
	public void fahreRueckwaertsZeit(float zeitInSek);
	
	public void dreheRechts();
	
	public void dreheRechtsGrad(int winkel);
	
	public void dreheRechtsZeit(float zeitInSek);
	
	public void dreheLinks();
	
	public void dreheLinksGrad(int winkel);
	
	public void dreheLinksZeit(float zeitInSek);
	
	public void beschleunige(int zeitInMS);
	
	public void stoppe();
}
