import lejos.nxt.Button;

/**
 * description Testklasse zur Demonstration der N�tzlichkeit von Interfaces
 * 
 * @author Ehlert / Hafezi / G�kcen / Trutz OSZ IMT
 * @version 1.0 / 08.10.2008
 * @version 1.1 / 22.01.2013
 * @version 1.2 / 09.01.2019
 */

public class FahrzeugTest {

	// Anfang Attribute
	// Ende Attribute

	// Anfang Methoden
	public static void main(String[] args) {

		Button.ENTER.waitForPressAndRelease();
		// Speed-Default-Wert ist 360
		FahrzeugInterface karsten = new Fahrzeug();
		// Fahrzeugnamen �ndern

		// 1. Aufgabe
		for (int i = 1; i <= 2; i++) {
			karsten.fahreVorwaertsStrecke(50);
			karsten.dreheRechtsGrad(90);
			karsten.fahreVorwaertsStrecke(20);
			karsten.dreheRechtsGrad(90);
		}

		// 2. Aufgabe
		for (int i = 1; i <= 36; i++) {
			karsten.fahreVorwaertsStrecke(5);
			karsten.stoppe();
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
			}
			karsten.dreheLinksGrad(10);
			karsten.stoppe();
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
			}
		}

		 // 3. Aufgabe
		karsten.setGeschwindigkeit(600, 600);
		for (int i = 1; i <= 2; i++) {
			karsten.fahreVorwaertsStrecke(50);
			karsten.dreheRechtsGrad(90);
			karsten.fahreVorwaertsStrecke(20);
			karsten.dreheRechtsGrad(90);
		}

//		 4. Aufgabe
//		 Fahrzeug f�hrt 10s vorw�rts, dabei "beschleunigt" es,
//		 anschlie�end bleibt es 2s stehen und dann f�hrt es mit
//		 kleinster Geschwindigkeit 5s r�ckw�rts
		karsten.beschleunige(10000);
		karsten.stoppe();
		karsten.setGeschwindigkeit(10, 10);
		karsten.fahreRueckwaertsZeit(5);
	}
	// Ende Methoden

}