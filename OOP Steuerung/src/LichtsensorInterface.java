
public interface LichtsensorInterface {
	public void kalibriereWerte();

	public boolean istSchwarzerBereich();

	public boolean istWeisserBereich();

	public void lichtSensorEin();

	public void lichtsensorAus();
}
