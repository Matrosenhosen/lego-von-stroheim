import lejos.nxt.Button;
import lejos.nxt.LightSensor;
import lejos.nxt.SensorPort;
import lejos.nxt.Sound;

public class SensorFahrzeug extends Fahrzeug implements LichtsensorInterface {

	LightSensor licht;
	int schwarzerWert;
	int weisserWert;

	public SensorFahrzeug() {
		super();
		SensorPort light = SensorPort.S1;
		licht = new LightSensor(light);

	}

	public int getSchwarzerWert() {
		return schwarzerWert;
	}

	public void setSchwarzerWert(int schwarzerWert) {
		this.schwarzerWert = schwarzerWert;
	}

	public int getWeisserWert() {
		return weisserWert;
	}

	public void setWeisserWert(int weisserWert) {
		this.weisserWert = weisserWert;
	}

	@Override
	public void kalibriereWerte() {
		Button.ENTER.waitForPressAndRelease();
		setWeisserWert(licht.readValue());
		this.zeigeZahl(weisserWert);
		Sound.beep();
		Button.ENTER.waitForPressAndRelease();
		setSchwarzerWert(licht.readValue());
		this.zeigeZahl(schwarzerWert);
		Sound.beep();
		Button.ENTER.waitForPressAndRelease();
	}

	@Override
	public boolean istSchwarzerBereich() {
		if (licht.readValue() <= getSchwarzerWert() + 4)
			return true;
		return false;
	}

	@Override
	public boolean istWeisserBereich() {
		if (licht.readValue() >= getWeisserWert() - 2)
			return true;
		return false;
	}

	@Override
	public void lichtSensorEin() {
		licht.setFloodlight(true);

	}

	@Override
	public void lichtsensorAus() {
		licht.setFloodlight(false);
	}

}
