import java.util.Random;

public class SensorTest {


		public static void main(String[] args) {
			SensorFahrzeug robert = new SensorFahrzeug();
			
			robert.kalibriereWerte();

			while (true) {
				robert.zeigeZahl(robert.licht.readValue());
					while (robert.istWeisserBereich()) {
						fahreVorwaertsStrecke(15,robert);;
						linksRechtsdrehen(robert);
					}
					
					rueckwaerts(robert);
			}
		}

		public static void rueckwaerts(SensorFahrzeug rob) {
			rob.zeigeZahl(rob.licht.readValue());
			rob.fahreRueckwaerts();
			
			try {
				Thread.sleep(750);
			} catch (InterruptedException e1) {
			}
			Random r = new Random();
			if (r.nextInt(2) == 0) {
				dreheRechts(r.nextInt(360) + 1,rob);
			} else {
				dreheLinks(r.nextInt(360) + 1,rob);
			}
		}

		public static void dreheRechts(int grad, SensorFahrzeug rob) {
			rob.dreheRechts();
			grad = grad * 100;
			grad = grad * 1933;
			grad = grad / 360;
			grad = grad / 10000;
			for (int i = 0; i < 100; i++)
				try {
					Thread.sleep(grad);
					if (rob.istSchwarzerBereich()) {
						rueckwaerts(rob);
					}
				} catch (InterruptedException e) {
				}
		}

		public static void dreheLinks(int grad, SensorFahrzeug rob) {
			rob.dreheLinks();
			grad = grad * 100;
			grad = grad * 1933;
			grad = grad / 360;
			grad = grad / 10000;
			for (int i = 0; i < 100; i++)
				try {
					Thread.sleep(grad);
					if (rob.istSchwarzerBereich()) {
						rueckwaerts(rob);
					}
				} catch (InterruptedException e) {
				}
		}

		// 1Meter = 5870
		public static void fahreVorwaertsStrecke(int streckeInCm, SensorFahrzeug rob) {
			streckeInCm = streckeInCm * 5870;
			streckeInCm = streckeInCm / 100;
			fahreVorwaertsZeit(streckeInCm,rob);
		}

		public static void fahreVorwaertsZeit(float zeitInSek, SensorFahrzeug rob) {
			rob.fahreVorwaerts();
			zeitInSek = zeitInSek/100;
			for (int i = 0; i < 100; i++) {
				try {
					Thread.sleep((int) zeitInSek);
					if (rob.istSchwarzerBereich()) {
						rueckwaerts(rob);
					}
				} catch (InterruptedException e) {
				}
			}
			rob.stoppe();
		}

		public static void linksRechtsdrehen(SensorFahrzeug roboter) {
			dreheLinks(30,roboter);
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
			}
			dreheRechts(60,roboter);
			dreheLinks(30,roboter);
	}

}
