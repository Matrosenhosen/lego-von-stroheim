import lejos.nxt.Button;

public class Rennen {

	static Boolean kreisrichtung; // true = links; false = rechts
	static boolean istLinie = true;
	static SensorFahrzeug rob = new SensorFahrzeug();
	static int[] zeit = new int[100];
	static int index = 0;

	public static void main(String[] args) {
		Button.ENTER.waitForPressAndRelease();
		while (index<100) {
			rob.fahreVorwaerts();
			while (rob.istSchwarzerBereich()) {
				try {
					Thread.sleep(5);
				} catch (InterruptedException e) {
				}
			}
			rob.stoppe();
			if (kreisrichtung != null)
				findeLinie();
			else
				sucheLinie();

		}
	}

	public static void findeLinie() {
		float s[] = {rob.getGeschwindigkeitLinks(),rob.getGeschwindigkeitRechts()};
		 rob.setGeschwindigkeit(265.2f, 270);
		if (kreisrichtung) {
			rob.dreheLinks();
			while (rob.istWeisserBereich()) {
				try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
				}
			}
			rob.stoppe();
		} else {
			rob.dreheRechts();
			while (rob.istWeisserBereich()) {
				try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
				}
			}
			rob.stoppe();
		}
		rob.setGeschwindigkeit(s[0], s[1]);
	}

	public static void sucheLinie() {
		 float s[] = {rob.getGeschwindigkeitLinks(),rob.getGeschwindigkeitRechts()};
		 rob.setGeschwindigkeit(265.2f, 270);
		rob.dreheLinks();
		for (int i = 0; i < 163 && kreisrichtung == null; i++) {
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
			}
			if (rob.istSchwarzerBereich()) {
				rob.stoppe();
//				if (i < 100)
//					kreisrichtung = true;
//				else
//					break;
				zeit[index] = i;
				break;
			}
		}
		if (rob.istWeisserBereich()) {
			rob.dreheRechts();
			for (int i = 0; i < 326 && kreisrichtung == null; i++) {
				try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
				}
				if (rob.istSchwarzerBereich()) {
					rob.stoppe();
//					if (i < 263)
//						kreisrichtung = false;
//					else
//						break;
					zeit[index] = i;
					break;
				}
			}
		}
		 rob.setGeschwindigkeit(s[0], s[1]);
	}

}
