import lejos.nxt.Button;

public class RennenZwei {
	static SensorFahrzeug rob = new SensorFahrzeug();
	static int speed = 600;
	static final float DREH_FAKTOR = (float) (speed * 0.35);

	public static void main(String[] args) {
		Button.ENTER.waitForPressAndRelease();
		rob.setGeschwindigkeit(DREH_FAKTOR, speed);
		rob.fahreVorwaerts();
		while (true) {
			rob.setGeschwindigkeit(DREH_FAKTOR, speed);
			rob.LampeLinks(true);
			rob.LampeRechts(false);
			while(rob.istWeisserBereich()) {
				wait(1);
			}
			while (rob.istSchwarzerBereich()) {
				wait(1);
			}
			rob.setGeschwindigkeit(speed,DREH_FAKTOR);
			rob.LampeLinks(false);
			rob.LampeRechts(true);
			while(rob.istWeisserBereich()) {
				wait(1);
			}
			while (rob.istSchwarzerBereich()) {
				wait(1);
			}
		}
	}
	
	public static void wait(int timeInMillis) {
		try {
			Thread.sleep(timeInMillis);
		} catch (InterruptedException e) {
		}
	}

}
