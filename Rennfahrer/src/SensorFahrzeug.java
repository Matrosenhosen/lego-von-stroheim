import lejos.nxt.LightSensor;
import lejos.nxt.SensorPort;

public class SensorFahrzeug extends Fahrzeug{

	LightSensor licht;
	int weisserWert = 40;
	LightSensor lampRechts;
	LightSensor lampLinks;

	public SensorFahrzeug() {
		super();
		SensorPort light = SensorPort.S1;
		licht = new LightSensor(light);
		lampRechts = new LightSensor(SensorPort.S2);
		lampLinks = new LightSensor(SensorPort.S3);
		lampRechts.setFloodlight(true);
		lampLinks.setFloodlight(true);
		lampRechts.readValue();
		lampLinks.readValue();
	}



	public boolean istSchwarzerBereich() {
		if (licht.readValue() < 40)
			return true;
		return false;
	}

	public boolean istWeisserBereich() {
		if (licht.readValue() >= 40)
			return true;
		return false;
	}

	public void lichtSensorEin() {
		licht.setFloodlight(true);

	}

	public void lichtsensorAus() {
		licht.setFloodlight(false);
	}
	public void LampeRechts(boolean b) {
		lampRechts.setFloodlight(b);
	}
	public void LampeLinks(boolean b) {
		lampLinks.setFloodlight(b);
	}
}
