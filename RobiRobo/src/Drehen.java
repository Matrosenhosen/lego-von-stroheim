import lejos.nxt.Button;
import lejos.nxt.Motor;

public class Drehen {

	public static void main(String[] args) {
		Button.ENTER.waitForPressAndRelease();

		Motor.A.setSpeed(360);
		Motor.B.setSpeed(360);
		
		Motor.A.backward();
		Motor.B.forward();
		try {
			Thread.sleep(1933);
		} catch (InterruptedException e) {
		}

		Motor.A.stop(true);
		Motor.B.stop();

	}

}
