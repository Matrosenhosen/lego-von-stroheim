import lejos.nxt.Button;
import lejos.nxt.Motor;

public class Kreis {
	public static void main(String[] args) {
		Button.ENTER.waitForPressAndRelease();

		Motor.A.setSpeed(361);
		Motor.B.setSpeed(180);

		Motor.A.forward();
		Motor.B.forward();

		try {
			Thread.sleep(7680);
		} catch (InterruptedException e) {
		}
		Motor.A.stop(true);
		Motor.B.stop();

	}
}
