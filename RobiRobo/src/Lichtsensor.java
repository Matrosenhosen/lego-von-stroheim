import lejos.nxt.LCD;
import lejos.nxt.LightSensor;
import lejos.nxt.SensorPort;

public class Lichtsensor {

	public static void main(String[] args) {
		SensorPort light = SensorPort.S1;
		LightSensor l = new LightSensor(light);
		
		while(true) {
			LCD.drawInt(l.readValue(), 0, 0);
		}
	}

}
