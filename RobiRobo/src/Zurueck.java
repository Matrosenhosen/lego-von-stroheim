import lejos.nxt.Button;
import lejos.nxt.Motor;

public class Zurueck {

	public static void main(String[] args) {
		Button.ENTER.waitForPressAndRelease();
		Motor.A.setSpeed(180);
		Motor.B.setSpeed(176.8f);
		
		Motor.A.backward();
		Motor.B.backward();
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {}
				
		Motor.A.stop(true);
		Motor.B.stop();
 


	}

}
