import lejos.nxt.Button;
import lejos.nxt.Motor;
import lejos.nxt.Sound;

public class Karsten {

	public static void main(String[] args) {
		Button.ENTER.waitForPressAndRelease();
		Motor.A.setSpeed(180);
		Motor.B.setSpeed(180);
		//spazieren
		Motor.A.forward();
		Motor.B.forward();
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
		}
		//treff auf Stahl
		Motor.A.stop(true);
		Motor.B.stop();

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
		}
		//Park gehen
		Motor.A.backward();
		Motor.B.forward();

		try {
			Thread.sleep(1933);
		} catch (InterruptedException e) {
		}

		Motor.A.forward();
		Motor.B.forward();

		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
		}
		//Jagen
		Motor.A.setSpeed(361);
		Motor.B.setSpeed(180);

		Motor.A.forward();
		Motor.B.forward();
		try {
			Thread.sleep(7680);
		} catch (InterruptedException e) {
		}
		//kaputt gehen
		Motor.A.stop(true);
		Motor.B.stop();

		Sound.playNote(Sound.PIANO, 264, 10000);
		//wieder ganz
		Motor.A.setSpeed(361);
		Motor.B.setSpeed(180);

		Motor.A.forward();
		Motor.B.forward();
		try {
			Thread.sleep(7680);
		} catch (InterruptedException e) {
		}

	}

}
